
<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
</head>
<body>

<h2>View Booking Details</h2>
<table style="width:60%">
<form action="ViewBooking" method="post">
  <tr>
    <th></th>
    <th>Date</th> 
    <th>StartTime</th>
    <th>EndTime</th>
    <th>No of Persons</th>
    <th>Table No</th>
  </tr>
  <tr>
    
<td><a  href="#" class="btn btn-primary a-btn-slide-text mybtn">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Delete</strong></span>          
                </a> </td>
    <td>3/3/19</td>
    <td>3:00:25AM</td>
    <td>4:00:30PM</td>
    <td>4</td>
    <td>1</td>  
  </tr>
  <tr>
    <td><a  href="#" class="btn btn-primary a-btn-slide-text mybtn">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Delete</strong></span>          
                </a> </td>
    <td>4/6/19</td>
    <td>8:00:40AM </td>
    <td>10:00:60PM</td>
    <td>3</td>
    <td>2</td>
  </tr>
  <tr>
  <td><a  href="#" class="btn btn-primary a-btn-slide-text mybtn">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Delete</strong></span>          
                </a> </td>
    <td>29/8/19</td>
    <td>6:00:30AM</td>
    <td> 9:00:20PM</td>
    <td>8</td>
    <td>4</td>
  </tr>
   <tr>
    
<td><a  href="#" class="btn btn-primary a-btn-slide-text mybtn">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Delete</strong></span>          
                </a> </td>
    <td>29/6/19 </td>
    <td>11:00:30AM </td>
    <td>2:00:30PM </td>
    <td>3</td>
    <td>2</td>
  </tr>
 
</table>

</body>
</html>
