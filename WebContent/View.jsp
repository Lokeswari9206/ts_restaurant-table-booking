
<html>
    <head>
<style>
* {box-sizing: border-box;}

body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav .search-container {
  float: right;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
  #Operation of ItemId{
      border: 3px solid black; 
  }
}
</style>
    </head>
    <body>
        <form action="AdminHome.jsp" method="post">
<div class="topnav">
  <a class="active" href="Item.jsp">Item</a>
    <a href="Logout.jsp">Logout</a>
  <div class="search-container">
      <input type="text" placeholder="Search.." name="search">
      <button type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div>
</div>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>

<%
                Class.forName("oracle.jdbc.driver.OracleDriver");
Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1522:xe", "system", "666");
                System.out.println("result"+conn);
Statement st = conn.createStatement();
ResultSet rs = st.executeQuery("select DishName,Price,image,Description from Item");
               System.out.println("result"+rs);
%>
<div id="displayimage">
    <table id="Operation of ItemId" class="table table-bordered" style="width:66.3%; border: 3px solid black;margin-left: 138px;">
        <tbody>
         <% while(rs.next()){  %>
       <% byte[] imgData = rs.getBytes("Image"); 
        System.out.println("bytes image: "+imgData);
        if(imgData != null)
        {
            String encode = Base64.getEncoder().encodeToString(imgData);
            request.setAttribute("imgBase", encode);
        }
      %>
<tr>
            <td>
                <img src="data:image/jpeg;base64,${imgBase}" alt="" width="210"
                     height="200" class="image"/></td>
            <td><%=rs.getString("DishName")%></br><%=rs.getString("Price")%></br></br><%=rs.getString("Description")%></td>
            <td><a  href="#" class="btn btn-primary a-btn-slide-text mybtn">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Item</strong></span>          
                </a> </td>
</tr>               
          <%}%>  
      </tbody> 
    </table>
</body>
</html>