package DAO;


import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Bean.CustomerRegisteration;

import Connector.DBconnection;

public class CustomerRegistrationDao 
{

	public static int Registration(CustomerRegisteration customerregistration) // To insert data into Database for signup
	{
		int status;
		
	try {

			Connection con = DBconnection.createConnection();
			System.out.println("conn");

			PreparedStatement ps = con.prepareStatement("insert into Registration(UserName,Password,ConfirmPassword,MobileNumber,Email) values(?,?,?,?,?)");
			
			ps.setString(1, customerregistration.getUserName());
			
           ps.setString(2, customerregistration.getPassword());
			
			ps.setString(3, customerregistration.getConfirmPassword());
			
			ps.setString(4, customerregistration.getMobileNumber());
			
			ps.setString(5, customerregistration.getEmail());
			
			status = ps.executeUpdate();
			
			if (status > 0) 
			
			{
				return status;

			}
		}
		
		
	catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return 0;
	}
	
	public String authenticateUser(CustomerRegisteration CustomerRegisteration) 
																// To authenticate user for log in
	{

		String UserName = CustomerRegisteration.getUserName();

		String password = CustomerRegisteration.getPassword();

		Connection con;

		Statement statement;

		ResultSet resultSet;

		String UserNameDB;

		String passwordDB;

		try {

			con = DBconnection.createConnection();

			statement = con.createStatement();

			resultSet = statement.executeQuery("select UserName,Password from Registration");

			
			while (resultSet.next()) 
			{

				UserNameDB = resultSet.getString("UserName");

				passwordDB = resultSet.getString("password");
			
				if (UserName.equals(UserNameDB) && password.equals(passwordDB)) 
				
				{
					return "SUCCESS";
					
				
				}
	
			}

		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return "Invalid user credentials";
	}

}





