
package DAO;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Bean.RegisterBean;
import Connector.DBconnection;

import java.sql.Connection;
import java.sql.DriverManager;

public class RegisterDao 
{

	public int Register(RegisterBean  RegisterBean) // To insert data into Database for signup
	{
		int status;

	try {

			Connection con = DBconnection.createConnection();

			PreparedStatement ps = con.prepareStatement("insert into REG(firstname,username,password,email,Retypepassword) values(?,?,?,?,?)");

			ps.setString(1, RegisterBean.getfirstname());
			ps.setString(2, RegisterBean.getuserName());
			ps.setString(3, RegisterBean.getPassword());
			ps.setString(4, RegisterBean.getemail());
			ps.setString(5, RegisterBean.getRetypepassword());
			
			status = ps.executeUpdate();
			
			if (status > 0) 
			
			{
				return status;

			}
		}
		
		
	catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return 0;
	}
	
	public String authenticateUser(RegisterBean RegisterBean) 
																// To authenticate user for log in
	{

		String firstname = RegisterBean.getfirstname();
		String username = RegisterBean.getuserName();
		String password = RegisterBean.getPassword();
		String email = RegisterBean.getemail();
		String Retypepassword = RegisterBean.getRetypepassword();

		Connection con;

		Statement statement;

		ResultSet resultSet;

		String userNameDB;

		String passwordDB;

		try {

			con = DBconnection.createConnection();

			statement = con.createStatement();

			resultSet = statement.executeQuery("select username,password from REG");

			while (resultSet.next()) 
			{

				userNameDB = resultSet.getString("username");

				passwordDB = resultSet.getString("password");
				
				if (firstname.equals(userNameDB) && password.equals(passwordDB)) 
				
				{
					return "SUCCESS";
				}
			}

		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return "Invalid user credentials";
	}

}