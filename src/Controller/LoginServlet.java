
package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.LoginBean;
import DAO.LoginDao;

@WebServlet(urlPatterns="/LoginServlet",name="LoginServlet")
public class LoginServlet extends HttpServlet {
	
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 
		 System.out.println("servlet");
String userName = request.getParameter("username");
System.out.println("username"+userName);
 String password = request.getParameter("password");
 System.out.println("password"+password);
 
LoginBean loginBean = new LoginBean();
 
loginBean.setusername(userName); 
System.out.println("setting");
 loginBean.setPassword(password);
 System.out.println("settig password");
LoginDao loginDao = new LoginDao();

 
String userValidate = loginDao.authenticateUser(loginBean); 
 
if(userValidate.equals("SUCCESS")) 
 {
 request.setAttribute("username", userName); 
 request.getRequestDispatcher("/Register.jsp").forward(request, response);
 }
 else
 {
 request.setAttribute("errMessage", userValidate);
 request.getRequestDispatcher("/LoginPage.jsp").forward(request, response);
 }
 
	}

}
