
package Controller;


import Bean.ItemBean;
import DAO.ItemDao;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author lenovo-pc
 */
@WebServlet(name = "ItemServlet", urlPatterns = "/add")
@MultipartConfig(maxFileSize = 16177216)
public class ItemServlet extends HttpServlet {
private static final long serialVersionUID = 1L;
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       System.out.println("books servlet called");
       InputStream inputStream = null; 
       String DishName = request.getParameter("DishName").trim();
       System.out.println("2222222222 "+DishName);
       String Price = request.getParameter("Price").trim();
       System.out.println("333333333 "+Price);
       Part part = request.getPart("image");
       System.out.println("44444444_image_ "+part);
       inputStream = part.getInputStream();
       System.out.println("555555555_image_ "+part);
    String Description= request.getParameter("Description").trim();
     System.out.println("88888888888"+Description);
        ItemBean instance = new ItemBean();
         instance.setDishName(DishName);
        instance.setPrice(Price);
        instance.setBytes(inputStream);
        instance.setDescription(Description);
        ItemDao ItemDao = new ItemDao();
             String returnResponse =  ItemDao.user(instance);
            System.out.println("Add Item connection return in servlet "+returnResponse);    
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}