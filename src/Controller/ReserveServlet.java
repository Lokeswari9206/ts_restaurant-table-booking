

	package Controller;

	import java.io.IOException;


	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;

	import javax.servlet.http.HttpServletRequest;

	import javax.servlet.http.HttpServletResponse;

	
import Bean.ReserveBean;
import Connector.DBconnection;

	
import DAO.ReserveDao;
	@WebServlet(urlPatterns="/ViewCustomer", name="ReserveServlet")



	public class ReserveServlet extends HttpServlet

	{

		private static final long serialVersionUID = 1L;

		protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
		{
																//getting value for registration of customer details
			String DateofBooking = request.getParameter("DateofBooking");
					System.out.println("111111111111"+DateofBooking);
			String Timetobook= request.getParameter("Timetobook");
			
			String  Persons = request.getParameter("Persons");
			
			String TableNumber = request.getParameter("TableNumber");
			
			ReserveBean Reserve = new ReserveBean(); //setting values to CustomerRegisterationBean

			Reserve.setDateofBooking(DateofBooking);
			Reserve.setTimetobook(Timetobook);
			Reserve.setPersons(Persons);
			Reserve.setTableNumber(TableNumber);
			



			ReserveDao ReserveDao = new ReserveDao(); //getting result from CustomerRegistrationDao

			String userValidate = ReserveDao.authenticateUser(Reserve);

			if (userValidate.equals("SUCCESS")) 
			{

				request.setAttribute("subscribedd", true);

				request.getRequestDispatcher("/ViewCustomer.jsp").forward(request, response);
				

			} 
			else 
			{

				request.setAttribute("subscribedd", false);

				request.getRequestDispatcher("/Reserve.jsp").forward(request, response);
			}
		}

	}

