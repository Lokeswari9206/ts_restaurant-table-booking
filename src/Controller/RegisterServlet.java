
package Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import Bean.RegisterBean;
import DAO.RegisterDao;
@WebServlet(urlPatterns="/Register",name="RegisterServlet")
public class RegisterServlet extends HttpServlet

{

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
															//getting value for registration of customer details
		String firstname = request.getParameter("firstname");
	     
		String username = request.getParameter("username");
		
		String password = request.getParameter("Password");
		
		String email = request.getParameter("enail");
		String Retypepassword = request.getParameter("Retypepassword");

		RegisterBean RegisterBean = new RegisterBean(); //setting values to CustomerRegisterationBean

		RegisterBean.setfirstname(firstname);
		RegisterBean.setuserName(username);
		RegisterBean.setpassword(password);
		
		RegisterBean.setemail(email);
		
		RegisterBean.setRetypepassword(Retypepassword);
		RegisterDao RegisterDao = new RegisterDao(); //getting result from CustomerRegistrationDao

		int userValidate = RegisterDao.Register(RegisterBean);

		if (userValidate > 0) 
		{

			request.setAttribute("subscribedd", true);

			request.getRequestDispatcher("/LoginPage.jsp").forward(request, response);

		} 
		else 
		{

			request.setAttribute("subscribedd", false);

			request.getRequestDispatcher("/Register.jsp").forward(request, response);
		}
	}

}