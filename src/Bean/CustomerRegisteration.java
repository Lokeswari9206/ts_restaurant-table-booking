package Bean;

public class CustomerRegisteration {
    private String UserName;
  
	private String Password;
	
	private String ConfirmPassword;
	
	private String MobileNumber;
	private String Email;
	
	public String getUserName() 
	{
		return  UserName;
	}

	public void setUserName(String  UserName) 
	{
		this. UserName= UserName;
	}

	public String getPassword() 
	{
		return Password;
	}

	public void setPassword(String Password) 
	{
		this.Password = Password;
	}

	public String getConfirmPassword() 
	{
		return ConfirmPassword;
	}

	public void setConfirmPassword(String ConfirmPassword) 
	{
		this.ConfirmPassword = ConfirmPassword;
	}

	public String getMobileNumber() 
	{
		return MobileNumber;
	}

	public void setMobileNumber(String MobileNumber) 
	{
		this.MobileNumber = MobileNumber;
	}
	public String getEmail() 
	{
		return  Email;
	}

	public void setEmail(String  Email) 
	{
		this. Email= Email;
	}


}