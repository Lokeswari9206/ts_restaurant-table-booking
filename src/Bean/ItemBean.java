
package Bean;

import java.io.InputStream;

public class ItemBean
{
    private String DishName;
   private String Price;
    private String Description;
    private InputStream  image;

public String getDishName() {
		return DishName;
	}
	public void setDishName(String DishName) {
		this.DishName = DishName;	
	}
	
	public String getPrice() {
		return Price;
	}
	public void setPrice(String Price) {
		this.Price = Price;	
	}
	public InputStream getBytes() 
	{
		return image;
	}

	public void setBytes(InputStream  Image) 
	{
		this.image = Image;
	}
	
	
	public String getDescription() {
		return Description;
	}
	public void setDescription(String Description) {
		this.Description = Description;	
	}


}